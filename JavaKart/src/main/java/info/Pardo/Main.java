package info.Pardo;

import info.Pardo.Race;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/**
 * 
 */

/**
 * @author Pardo
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		 ApplicationContext context = new ClassPathXmlApplicationContext("Javakart.xml");
	        
	        Race race = (Race) context.getBean("race");
	        
	        System.out.println(race.toString());
	        race.run();
	        System.out.println(race.showResult());

		}

}
