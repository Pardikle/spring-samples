/**
 * 
 */
package info.Pardo;

/**
 * Inits JavaKart race
 * @author Pello Altadill
 * @greetz for the blue mug
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("javakart.xml");
        
        Race race = (Race) context.getBean("race");
        
        System.out.println(race.toString());
        race.run();
        System.out.println(race.showResult());

	}

}
